---
title: Vue CheatSheet for Begginners
created: '2019-10-31T13:49:23.266Z'
modified: '2019-12-10T19:01:17.998Z'
---

# Vue CheatSheet for Begginners

- https://github.com/sdras/intro-to-vue
- https://riptutorial.com/vue-js/topic/1057/getting-started-with-vue-js 
- https://github.com/vueschool/dynamic-forms
- virtual dom implementation - https://github.com/snabbdom/snabbdom

!!! most useful benefit of virtual DOM is render function... Writing your logic into the DOM has benefits not the VDOM itself. VDOM is slower than manupulating DOM. we need patch and diff function. Evan you says.

### Directives
| directive        |  doc |
|-----------------------| ---- |
|[`v-text`](https://vuejs.org/v2/api/#v-text)| Similar to using mustache templates `{{tacos}}` or `<p v-text="tacos"></p>`|
|[`v-html`](https://vuejs.org/v2/api/#v-html)| Great for strings that have html elements that need to be rendered!|
|[`v-show`](https://vuejs.org/v2/api/#v-show)|  changes visibility also is a conditional that will display information depending on meeting a requirement. This can be anything- buttons, forms, divs, components.|
|[`v-if`](https://vuejs.org/v2/api/#v-if)| mount or unmount the element from the DOM
|[`v-else`](https://vuejs.org/v2/api/#v-else)| Pretty straightforward- you can conditionally render one thing or another. There's also v-else-if
|[`v-else-if`](https://vuejs.org/v2/api/#v-else-if)|
|[`v-for`](https://vuejs.org/v2/api/#v-for)| Loops through a set of values. Can also do a static number
|[`v-on`](https://vuejs.org/v2/api/#v-on)|Extremely useful so there's a shortcut! v-on is great for binding to events like click and mouseenter. You're able to pass in a parameter for the event like (e). We can also use ternaries directly `@click="counter > 0 ? counter -= 1 : 0`|
|[`v-bind` or `:`](https://vuejs.org/v2/api/#v-bind)| One of the most useful directives so there's a shortcut! We can use it for so many things- class and style binding, creating dynamic props, etc...|
[`v-model`](https://vuejs.org/v2/api/#v-model)| Creates a relationship between the data in the instance/component and a form input, so you can dynamically update values
|[`v-pre`](https://vuejs.org/v2/api/#v-pre)|v-pre will print out the inner text exactly how it is, including code (good for documentation)|
|[`v-cloak`](https://vuejs.org/v2/api/#v-cloak)|
|[`v-once`](https://vuejs.org/v2/api/#v-once)| Not quite as useful, v-once will not update once it's been rendered.
|source | http://slides.com/sdrasner/intro-to-vue-1?token=9-aFNhlX#/14|

<img src="https://s3.amazonaws.com/media-p.slid.es/uploads/75854/images/3964241/Screen_Shot_2017-07-04_at_8.01.02_PM.png">
<img src="https://s3.amazonaws.com/media-p.slid.es/uploads/75854/images/3964242/Screen_Shot_2017-07-04_at_8.01.18_PM.png">


### Modifiers
| modifier | doc |
|--------- | ----| 
|`v-model.trim`| will strip any leading or trailing whitespace from the bound string|
|`v-model.number`| changes strings to number inputs |
| `v-model.lazy`| won’t populate the content automatically, it will wait to bind until an event happens. (It listens to change events instead of input)|
|`@mousemove.stop`| is comparable to e.stopPropagation()|
|`@mousemove.prevent`|this is like e.preventDefault()|
|`@submit.prevent`|this will no longer reload the page on submission|
|`@click.once`| not to be confused with `v-once`, this click event will be triggered once.
|`@click.native`| so that you can listen to native events in the DOM|

<img src="https://gitlab.com/oyilmaztekin1/frontend-worlds-tips/raw/master/attachments/computed-methods.png">


### Life Cycle Hooks
<img src="https://s3.amazonaws.com/media-p.slid.es/uploads/75854/images/3975334/lifecycle.gif">
http://slides.com/sdrasner/intro-to-vue-4?token=Xb4oA4YR#/12


### Vue Reactivity System
**There are two ways to detect whether a value inside a JavaScript object has changed:**
- By using the Proxy feature, which is defined in ECMAScript 2015 (6th Edition, ECMA-262)
- By using Object.defineProperty, which is defined in ECMAScript 2011 (5.1 Edition, ECMA-262)

Vue will walk through all the properties of the data part and use `Object.defineProperty` to convert them into `getter/setter` methods. **For this reason, Vue can only detect changes to properties that _have been defined_ in the data part of a component.**

- https://github.com/d-levin/vue-advanced-workshop/tree/master/1-reactivity
- https://github.com/numbbbbb/read-vue-source-code/blob/master/03-init-introduction.md#initinjections
- https://github.com/numbbbbb/read-vue-source-code/blob/master/04-dynamic-data-observer-dep-and-watcher.md

#### Watchers
Watchs reactive streams... Calculations in JavaScript is cheap but accessing the DOM and manipulating is expensive... That is one of the Virtual DOM's benefit. 

The vue instance is the middleman between DOM and Logic 

Watch updates the DOM only if it's required- performing calculations in JS is really performant but accessing the DOM is not. So we have a Virtual DOM which is like a copy, but parsed in JavaScript

It will only update the DOM for things that need to be changed, which is faster.

Good for asynchronous updates, and updates/transitions with data changes

http://slides.com/sdrasner/intro-to-vue-2?token=502n2b7V#/30

<img src="https://gitlab.com/oyilmaztekin1/frontend-worlds-tips/raw/master/attachments/watcher-vdom.png ">

## Components
there are two type of components
- Global Components
- Local Components

#### Global Components
can be used with `Vue.components()` it registers the component to the global. Even we don't use it still will be compressed in our build file. 

```javascript
  Vue.component('plain-component',{
    props:{},
    methods:{},
    ...
  })
```

#### Local Components
can be used as plain javasript object like 
```javascript
const plainComponent = {
  template: '#plain-component',
  props: {},
  methods:{}
  ...
}
```
[**HOC**](https://github.com/d-levin/vue-advanced-workshop/blob/master/3-render-function/3.3-higher-order-component.html) can be used with these kinds of local component 

## Communicating Event
`$emit`: The child is reporting it's activity to the parent
https://vuejs.org/v2/guide/components-custom-events.html#Event-Names

Additionally, `v-on` event listeners inside DOM templates will be automatically transformed to lowercase (due to HTML’s case-insensitivity), so `v-on:myEvent` would become `v-on:myevent` – making myEvent impossible to listen to.

For these reasons, we recommend you always use **kebab-case** for event names.

### Smarter Watcher
```js
created() {
 this.fetchUserList()
},
watch: {
  searchText() {
    this.fetchUserList()
  }
}
```
Watchers can take string as a value instead of a function. Handler will be called as soon as component is ready "immediatly". 
```js
created() {
 this.fetchUserList()
},
watch: {
  searchText: {
    handler: 'fetchUserList', // a string representation of a method
    immediate: true // we dont need to created hook anymore
  } 
  }
}
```

## Filters, Mixin, Custom Directives
http://slides.com/sdrasner/intro-to-vue-6?token=fcL8qgTg#/1

#### Filters
- can be used to apply common text formatting such as capitizing, uppercasing, reversing, formatting date etc...
- they aren't replacements for methods, computed values, or watchers, **because filters don't transform the data, just the output that the user sees.**
- Two type of filters can be created; locally and globally like component have
- are rerun on every single update **better to use computed**

```javascript
//global
Vue.filter('filterName', function(value) {
  return // thing to transform
});
 
//locally, like methods or computed
filters: {
  filterName(value) {
    return // thing to transform
  }
}
```
```html
<!-- in mustaches -->
{{ message | capitalize }}

<!-- in v-bind -->
<div v-bind:id="rawId | formatId"></div>
```

#### Mixin
- Can be used **OOP approaches**
- When components are sharing same logic
- Global mixin can be used where you may need to gain access everywhere across the application... **use case are extremly limited**


```javascript
// define a mixin object
var myMixin = {
  created: function () {
    this.hello()
  },
  methods: {
    hello: function () {
      console.log('hello from mixin!')
    }
  }
}

// define a component that uses this mixin
var Component = Vue.extend({
  mixins: [myMixin]
})

var component = new Component() // => "hello from mixin!"
```

### Custom Directive
- gives some flexibility about doing something on the before component mount such as adding listener on the component, changing styles etc...
```javascript 
Vue.directive('tack', {
  bind(el, binding, vnode) {
    el.style.position = 'fixed';
    el.style.top = binding.value.top + 'px';
    el.style.left = binding.value.left + 'px';
  }
}); 
```

```html
<p v-tack="{ top: '40', left: '100' }">Stick me 40px from the top of the
page and 100px from the left of the page</p>
```

## VUEX

#### Getters:
- getters can read the value with or without changing it inside of the getter function scope, but not mutate the state on the store...

#### Actions:
- Allow us to update the state **asynchronously**
- They represents **what can be done but not how it is**. 
- An action is a simple object and with a type and data. 
- It is not possible to change the Vuex state directly inside an action; instead, **actions commit mutations**. 
- You have to deal with asynchronous code inside actions, since **mutations are synchronous**.

```js 
actions: {
  increment (context) {
    context.commit('increment');
  }
}

//instead of
actions: {
  increment ({ commit } ) {
    commit('increment');
  }
}
```

#### Mutations
- Mutations **can and do modify the application state**. 
- They represent the application logic directly connected to the application state. 
- Mutations should be simple, **since complex behavior should be handled by actions**.
- They will always be synchronous. 
- only way to change data in the state in the store

#### Dispatcher
It is a simple mechanism to dispatch actions, and it can handle dependencies between stores by dispatching actions to the stores in a specific order. Since Vuex differs from Flux because the dispatcher is inside the store, what you should remember here is that every change in the application begins by dispatching an action.

#### Store

Stores contain the application state and logic. Stores can be mutated only by actions and do not expose any setter method. There can be more than one store in Flux, each one representing a domain within the application. In Vuex, there is only one store, and its state is called a single state tree.

**There are three important concepts regarding stores:**
- Stores can be mutated only by actions 
- Once a store is mutated, it notifies it has changed to the views 
- Stores represent explicit data, as opposed to deriving data from events

- camelcasing will be converted automaticly. HTML tags must be kebab-case  
- Each component has its own isolated scope, that is why we are putting `data` as a function
- html binding tarafında method kısmında tanımlanan fonksiyonlar çalıştırılabilir. `{{ callFunction() }}` gibi..  
- HTML tarafında fonksiyondan parametre geçirebiliriz. `{{ callFunction(1,param2) }}`
- HTML render etmek için template içerisinde `v-bind:href='param'` or `v-html='param'` kullanmamız gerekiyor.
- Sadece bir kere render etmek istiyorsak ve sonraki manipülasyonlardan etkilenmesini istemiyorsak `v-once`
- `event.stopPropagation();` işleminin html tarafındaki kısayolu `v-on:onmouse.stop`
- `event.preventDefault();` işleminin html tarafındaki kısayolu `v-on:onmouse.prevent`
- data içerisinde tanımlanmış bir property için expression tanımlayabiliriz. `v-on:click="counter++"` gibi.. veya `{{ counter++ }}` gibi
- memoization veya cache olarak `computed` metodları kullanabiliriz.
- reactivity: asenkron veya farklı bir değerin değişip değişmediğini kontrol etmek için `watch`…
- toggle işlemi için `@click='show = !show'`
- `v-if` negatif olduğu durumlarda objeyi gizlemez, tamamen DOM içerisinden siler.  attach ve detach eder. `v-show` ise `v-if` den farklı olarak objeyi `display:none;` eder. yani gizler. detach veya attach etmez. 
- `<div v-for ="(value, key,index) in persons"> {{key}}: {{value}} <div/>` obje içerisinde tüm property leri iterate eder.
- `v-for="n in 10" {{n}}` 10 kadar number tekrar eder.
- array ekleme `@click="array.push('value')"` bunun güvenli yolu value ya `:key` atamaktır. [sample](https://jsfiddle.net/smax/o7uy2g0u/)




