---
title: Rust
created: '2019-12-13T08:58:55.788Z'
modified: '2019-12-13T08:59:55.024Z'
---

# Rust

- By Mozilla developer podcast  -https://learning.oreilly.com/videos/the-rust-programming/9781491925447/9781491925447-video224092
- https://learning.oreilly.com/videos/learning-rust/9781788477918/continue 
- conference: https://learning.oreilly.com/videos/oscon-2018/9781492026075/9781492026075-video321453
- conference: https://learning.oreilly.com/videos/oscon-2017/9781491976227/9781491976227-video306635
