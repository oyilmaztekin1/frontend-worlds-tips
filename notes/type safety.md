---
title: type safety
created: '2019-12-13T07:20:29.505Z'
modified: '2019-12-13T12:35:59.200Z'
---

# type safety


**javascript neden dinamik, statik tip değil**
Many of the rapid development features found in scripting languages do not fit well with static typing. This allows code to be quickly hacked together and deployed to the customer.

**Programming language theory**

Bir dilin type safe olduğunun isbatı çok zor. Ciddi isbatlar istiyor.

- type safe example: c, c++, java, c#, 
- runtime aşamasında da type safety olabilir. runtime aşamasında tip denetimi yapar ve hataları tespit eder. JavaScript böyledir.  `TypeError` objesi bu hata denetim operasyonlarını temsil eder. http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.121.8387&rep=rep1&type=pdf
- compile aşamasındaki statik tip zorlaması... Güçlü Doktora tezi ve bilimsel çalışma...
- c ve c++ type safety olmadığını söylüyorlar. Çünkü C'de `TypeError` yok. ne var (syntax, runtime, semantic, logical)
- statik, dinamik, strong, weak, kavram kargaşası... 
- C checks at compile time, and is not type safe; Python checks at runtime, and is type safe.
- python, java, C++ tamamen tip güvenli değil çünkü çok büyükler ve tip hatalarının kayıp geçmesi çok mümkün.

-------
- Tip güvenliği aynı zamanda hafıza(memory) güvenliği ile de bağlantılı. Tip değişikliğinde hafızada lokasyon(pointer) değişikliği de yaşanıyor. immutable datalar low level dillerde mutable olarak başka bir fonksiyona referans olarak verilebiliyor. rust da `mut` C'de `*`(asterisk). Hem verinin o anki değerini hem de referans aldığı pointer değerini karşılaştırıp denetlemesi lazım. javascript de abstact data tipleri yok. (queue, stack, list, hash tables)  
-----

- Type-safe code accesses only the memory locations it is authorized to access

- strong-type: dönüşüm yok, weak: var.

- ne kadar kuralcı ve zorlayıcı olursa zaman uzuyor. zor,teknik ve best practice öğrendikten sonra birşeyler ortaya koymaya başlıyorsun. Güvenli kod yazma alışkanlığı kazanıyor.

### null pointer: 
- a reference doesn't refer a valid adress. (nil in lisp and go)
- variable that isn't assigned any valid memory adress yet
- must check null before accessing the any pointer
- in static type: must pass a null pointer to fn arg we dont want to pass valid memory adress

### Tony Hoare designer of ALGOL W language says about **null pointer**
> I call it my billion-dollar mistake. It was the invention of the null reference in 1965. At that time, I was designing the first comprehensive type system for references in an object oriented language (ALGOL W). My goal was to ensure that all use of references should be absolutely safe, with checking performed automatically by the compiler. But I couldn't resist the temptation to put in a null reference, simply because it was so easy to implement. This has led to innumerable errors, vulnerabilities, and system crashes, which have probably caused a billion dollars of pain and damage in the last forty years.

### fikir sansasyonel
- !!!katı bir dil ile başlayıp, sonrasında js yazmak daha güvenli.
- koşturmayalım, güvenli yazalım. 
- Statik tip, dinamikten daha güvenlidir ancak tamamen tip güvenli değildir. 
- Algoritma ile test ediliyor. Matematiksel ispat istiyor. (js için isbatlar pozitif)

https://stackoverflow.com/questions/46388355/is-python-type-safe





