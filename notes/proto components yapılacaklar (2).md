---
title: proto components yapılacaklar
created: '2020-02-02T16:57:21.731Z'
modified: '2020-02-02T16:57:32.158Z'
---

# proto components yapılacaklar

- [ ] eslint kuralları
- [ ] jshint veya css hint
- [ ] babel ayarları
- [ ] flow ayarlarını temizle
- [ ] virtual dom .jsx, logic .js
- [ ] component -> index.js yerine component -> component.jsx
- [ ] protypes kimi yerde static kimi yerde instance... standart
- [ ] unit testleri güncelle
- [ ] düzenlenebilir sketch dosyaları oluşturmak gerekiyor ( design system ) -  takas a tasarımcı sokup içeride halledebiliriz. Pazarlamada inanılmaz etkileyici olacaktır.
- [ ] for creating starter kits **yeoman** might be huge help( or  a cli )
- [ ] component structure creation tool with all neccessary folders and files
- [ ] sassdoc can be used for documenting styles - [https://www.oddbird.net/herman/docs/](https://www.oddbird.net/herman/docs/) & [http://sassdoc.com/](http://sassdoc.com/)

[- \[ \]](http://sassdoc.com/) Proje dökümantasyonu için - [https://www.wappalyzer.com/technologies/docusaurus](https://www.wappalyzer.com/technologies/docusaurus)

eksik componentler

- [ ] copy button - [http://react.carbondesignsystem.com/?path=/story/copybutton--default](http://react.carbondesignsystem.com/?path=/story/copybutton--default)
- [ ] multiselect - [http://react.carbondesignsystem.com/?path=/story/multiselect--default](http://react.carbondesignsystem.com/?path=/story/multiselect--default)
- [ ] overflowMenu - [http://react.carbondesignsystem.com/?path=/story/overflowmenu--custom-trigger](http://react.carbondesignsystem.com/?path=/story/overflowmenu--custom-trigger)
- [ ] progressIndicator or step - [http://react.carbondesignsystem.com/?path=/story/progressindicator--default](http://react.carbondesignsystem.com/?path=/story/progressindicator--default) / [https://ant.design/components/steps/](https://ant.design/components/steps/)
- [ ] rating - [https://ant.design/components/rate/](https://ant.design/components/rate/)
- [ ] result - [https://ant.design/components/result/](https://ant.design/components/result/)
- [ ] skeleton - [https://ant.design/components/skeleton/](https://ant.design/components/skeleton/) - [http://react.carbondesignsystem.com/?path=/story/skeletontext--default](http://react.carbondesignsystem.com/?path=/story/skeletontext--default)
- [ ] timeline - [https://www.lightningdesignsystem.com/components/activity-timeline/](https://www.lightningdesignsystem.com/components/activity-timeline/)
- [ ] visual picker - [https://www.lightningdesignsystem.com/components/visual-picker/](https://www.lightningdesignsystem.com/components/visual-picker/)
- [ ] interaction animation - no need to be informative but when something clicked should be response [https://material.io/design/motion/understanding-motion.html#principles](https://material.io/design/motion/understanding-motion.html#principles)
- [ ] transition works with react router
