---
title: Rewrite yaparken???
created: '2019-12-08T09:37:18.539Z'
modified: '2019-12-08T09:37:20.266Z'
---

## Rewrite yaparken???
- Sepeti yeniden yazarken sepet logicleri üzerinde geliştirme durdurulacak mı? ( kullanıcı ve business mutsuzluğu )
- Development devam edecek, mümkün olduğunca taşıma ve yeniden yazım yapılacak. (İnanılmaz yavaşlatır. senkron sağlanamayabilir ) 
- Development devam eder, eski koddaki yeni geliştirmeler ilk aşamada dahil edilmez. Yeniden yazım bittiğinde eski kodda geliştirmeler durdurulur ve kalan geliştirmeler yeni koda taşınır.
