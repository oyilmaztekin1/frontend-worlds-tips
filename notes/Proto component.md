---
title: Proto component
created: '2020-02-04T20:02:21.896Z'
modified: '2020-02-04T20:02:41.488Z'
---

# Proto component 
```js
import * as React from 'react';
import { ActivityIndicator, Platform, Text, TouchableHighlight, View } from 'react-native';
import PropTypes from 'prop-types';
import Icon from '@protonative/icon';
import ButtonGroup from './lib/group';
import { styles } from './assets/styles';

const loader = () => {
  return (
    <View style={styles.button_loading}>
      <ActivityIndicator size="small" color="white" />
    </View>
  );
};

class Button extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.handleClick = this.handleClick.bind(this);
    this.createMainStyle = this.createMainStyle.bind(this);
    this.createInnerStyle = this.createInnerStyle.bind(this);
    this.createTextStyle = this.createTextStyle.bind(this);
    this.createIconStyle = this.createIconStyle.bind(this);
    this.createUnderlayColor = this.createUnderlayColor.bind(this);
    this.checkIconPositionLeftOrTopAndPlace = this.checkIconPositionLeftOrTopAndPlace.bind(this);
    this.checkIconPositionRightOrBottomAndPlace = this.checkIconPositionRightOrBottomAndPlace.bind(this);


  }

  handleClick() {
    this.props.onClick && this.props.onClick();
    this.context && this.context.submit && this.context.submit();
  }

  createMainStyle() {
    const { className } = this.props;
    return className && className.style ? className.style : null;
  }

  createInnerStyle() {
    const { className } = this.props;
    return className && className.innerStyle ? className.innerStyle : null;
  }

  createTextStyle() {
    const { className } = this.props;
    return className && className.textStyle ? className.textStyle : null;

  }

  createIconStyle() {
    const { className } = this.props;
    return className && className.iconStyle ? className.iconStyle : null;

  }

  createUnderlayColor() {
    const { className } = this.props;
    return className && className.underlayColor ? className.underlayColor : '#00a0cc';

  }

  renderIcon(selector){
    const { iconName, secondary } = this.props;
    const iconColor = { iconColor: '#00a0cc' };
    return (
      <Icon
        data-test-selector={selector}
        className={secondary ? [iconColor, this.createIconStyle()] : this.createIconStyle()}
        iconName={iconName}
      />
    );
  }

  checkIconPositionLeftOrTop(){
    const { iconPosition } = this.props;
    return iconPosition === 'left' || iconPosition === 'top';
  }

  checkIconPositionRightOrBottom(){
    const { iconPosition } = this.props;
    return iconPosition === 'right' || iconPosition === 'bottom';
  }

  checkIconPositionLeftOrTopAndPlace() {
    if ( this.checkIconPositionLeftOrTop() ) {
      this.renderIcon("leftOrTopPositionIcon");
    }
    return null;

  }

  checkIconPositionRightOrBottomAndPlace(){
    if (this.checkIconPositionRightOrBottom()) {
      this.renderIcon("rightOrBottomPositionIcon");
    }
    return null;
  }

  render() {
    const {
      id,
      secondary,
      disabled,
      custom,
      light,
      fetching,
      twoLines,
      text,
      iconName,
      iconPosition
    } = this.props;

    return (
      <TouchableHighlight
        testID={Platform.OS === 'ios' ? id : undefined}
        accessibilityLabel={Platform.OS === 'android' ? id : undefined}
        onPress={this.handleClick}
        underlayColor={secondary ? null : this.createUnderlayColor()}
        disabled={disabled}
        style={[
          styles.container,
          custom ? styles.custom_button : null,
          disabled ? styles.button_inactive : null,
          light ? styles.button_light : null,
          fetching ? styles.button_fetching : null,
          secondary ? styles.button_secondary : null,
          twoLines ? styles.button_two_lines : null,
          this.createMainStyle()
        ]}
      >
        <View
          style={[
            styles.inner_container,
            iconPosition === 'left' ? styles.button_icon_left : null,
            iconPosition === 'right' ? styles.button_icon_right : null,
            iconPosition === 'top' ? styles.button_icon_top : null,
            iconPosition === 'bottom' ? styles.button_icon_bottom : null,
            this.createInnerStyle()
          ]}
          data-test-selector={'ButtonTouchableHighlightInnerView'}
        >
          {iconName && this.checkIconPositionLeftOrTopAndPlace()}

          <Text data-test-selector={'buttonInnerText'}
                style={[styles.text, secondary ? styles.button_secondary_text : null, this.createTextStyle()]}>
            {text}
          </Text>

          {iconName && this.checkIconPositionRightOrBottomAndPlace()}

          {iconName && (iconPosition === 'right' || iconPosition === 'bottom') ? (
            <Icon data-test-selector={'rightOrBottomPositionIcon'}
                  className={secondary ? [iconColor, this.createIconStyle()] : this.createIconStyle()}
                  iconName={iconName} />
          ) : null}
          {fetching ? loader() : null}
        </View>
      </TouchableHighlight>
    );
  }

}

Button.displayName = 'Button';


Button.propTypes = {
  text: PropTypes.string,
  disabled: PropTypes.bool,
  fetching: PropTypes.bool,
  secondary: PropTypes.bool,
  light: PropTypes.bool,
  twoLines: PropTypes.bool,
  iconName: PropTypes.string,
  iconPosition: PropTypes.oneOf([
    '',
    'left',
    'right',
    'top',
    'bottom'
  ]),
  custom: PropTypes.bool,
  id: PropTypes.string,
  className: PropTypes.any,
  onClick: PropTypes.func
};

Button.defaultProps = {
  iconPosition: 'left'
};

Button.contextTypes = {
  submit: PropTypes.func
};

Button.Group = ButtonGroup;
Button.Icons = Icon.Icons;

export default Button;


```
