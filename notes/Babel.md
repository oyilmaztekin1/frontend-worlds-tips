---
title: Babel
created: '2019-12-08T09:37:50.327Z'
modified: '2019-12-08T09:37:53.169Z'
---

# Babel
- where does name came from https://github.com/babel/babel/issues/596#issuecomment-71579638
- Babel is just a transpiler turns syntax to another syntax **ES6+ to ES5**...
- `preset-env` Compiling ES6+ to **natively supported JS**
- Production Configuration for babel... module system with webpack... https://philipwalton.com/articles/deploying-es2015-code-in-production-today/ 
- loader: https://github.com/babel/babel-loader/tree/8f240b498bb24ef89f7b306f5ac806e84b813b0d
