---
title: Better code
created: '2019-11-12T11:37:32.028Z'
modified: '2019-11-22T12:05:37.897Z'
---

# Better code

- Don’t be too quick to choose a name. Make sure the name is descriptive
- **Long name for long scope** The length of a name should be related to the length of the scope. You can use very short variable names for tiny scopes, but for big scopes you should use longer names.
- Variables and function should be defined close to where they are used.
- Local variables should be declared just above their first usage and should have a small vertical scope.
- If you do something a certain way, do all similar things in the same way
- General enum, static method should be contained in common.
- Function Names Should Say What They Do
- Replace Magic Numbers with Named Constants: `86,400` yerine `ITEM_PER_DAY = 86,400`  
- Prefer calling function istead of combining temporal in your class (G31: Hidden Temporal Couplings)
- If a structure appears arbitrary, others will feel empowered to change it
- Keep Configurable Data at High Levels
- Each unit should have only limited knowledge about other units: only units “closely” related to the current unit. Each unit should only talk to its friends; don’t talk to strangers. (G36: Avoid Transitive Navigation, Law of Demeter) 
- Don't use more than 2 or 3 parameter. If you have to split function into smaller or use **Builder Pattern** or object literal as mutable state (not recommend)


## Refactoring
- Try to understand logic
- Change bad naming
- Split functions 
- Run tests see are they fail 


## Commenting
> Don't comment, clean your code until comment is necessary

**BAD**
```java
// Check to see if the employee is eligible for full benefits
   if ((employee.flags & HOURLY_FLAG) &&
       (employee.age > 65))
```

**GOOD**
```javascript
   if (employee.isEligibleForFullBenefits())
```

```js
  // Copyright (C) 2003,2004,2005 by Object Mentor, Inc. All rights reserved.
  // Released under the terms of the GNU General Public License version 2 or later.
```

```java
   // Returns an instance of the Responder being tested.
   protected abstract Responder responderInstance();

   // format matched kk:mm:ss EEE, MMM dd, yyyy
   Pattern timeMatcher = Pattern.compile("\\d*:\\d*:\\d* \\w*, \\w* \\d*, \\d*");
   assertTrue(a.compareTo(a) == 0);    // a == a
   assertTrue(a.compareTo(b) != 0);    // a != b

    // Don't run unless you 
   // have some time to kill.
   public void _testWithReallyBigFile()
   {...

   //TODO: ....
```

### Objects and Data Structures
- module should not know the innards... in other words object shouldn't expose their data... (_Law of Demeter_)
- Always expose behavior and hide data...

### Error handling
- write try catch first. in other words write your error handlers first
- use conditional catch... (except js)
```js
try {
  myroutine(); // may throw three types of exceptions
} catch (e) {
  if (e instanceof TypeError) {
    // statements to handle TypeError exceptions
  } else if (e instanceof RangeError) {
    // statements to handle RangeError exceptions
  } else if (e instanceof EvalError) {
    // statements to handle EvalError exceptions
  } else {
    // statements to handle any unspecified exceptions
    logMyErrors(e); // pass exception object to error handler
  }
}
```
