---
title: Managing Styles and CSS
created: '2019-12-04T10:11:52.238Z'
modified: '2019-12-04T10:16:25.608Z'
---

# Managing Styles and CSS

### Managing styles with attributes

```html
<div class="example-container">
  <div 
    class="rh-card--layout" 
    data-rh-theme="dark"
    data-rh-justify="center"
  >...</div>
</div>
```

```css
<!-- card.scss -->
.rh-card--layout {
  padding: 30px;
  &[data-rh-theme="light"]{
    background: white;
  }
  &[data-rh-theme="dark"]{
    background: black;
  }
  &[data-rh-justify="center"]{
    ...
  }
  &[data-rh-justify="top"]{
    ...
  }
  &[data-rh-justify="justify"]{
    ...
  }
}
```

