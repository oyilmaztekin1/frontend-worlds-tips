---
title: JS Module Management
created: '2019-12-08T09:38:36.326Z'
modified: '2019-12-08T09:39:26.050Z'
---

# JS Module Management
- https://addyosmani.com/writing-modular-js/
- https://mail.mozilla.org/pipermail/es-discuss/2008-August/003400.html
- https://auth0.com/blog/javascript-module-systems-showdown/
- https://requirejs.org/
- https://dojotoolkit.org/ 
- https://github.com/cujojs/curl 
- https://github.com/getify/LABjs
- https://github.com/SlexAxton/yepnope.js
- https://github.com/requirejs/almond

### Polyfills and Module System
- https://polyfill.io/v3/url-builder/ pick your polyfill and download
- https://philipwalton.com/articles/loading-polyfills-only-when-needed/
- https://schepp.github.io/HTTP-headers


```html
<!-- browser which supports module import also supports ES6+ --> 
<!-- Browsers with ES module support load this file. -->
<script type="module" src="main.mjs"></script>

<!-- Older browsers load this file (and module-supporting -->
<!-- browsers know *not* to load this file). -->
<script nomodule src="main.es5.js"></script>
```

- Modules are loaded like `<script defer>`, which means **they aren’t executed until after the document has been parsed**. If some of your code needs to run before that, it’s best to split that code out and load it separately.

- Modules always run code in `strict mode`, so if for whatever reason any of your code needs to be run outside of strict mode, it’ll have to be loaded separately.

#### AMD (Asynchronous Module Definition)
- Implemented by RequireJs
- Used for the client side (browser) when you want dynamic loading of modules
- Import via “require”
- Complex Syntax



```js
//requireJS
require( [ window.JSON ? undefined : 'util/json2' ], function ( JSON ) {
  JSON = JSON || window.JSON;

  console.log( JSON.parse( '{ "JSON" : "HERE" }' ) );
});

//Dojo
define([
    "dojo/_base/declare",
    "dojo/dom",
    "app/dateFormatter"
], function(declare, dom, dateFormatter){
    return declare(null, {
        showDate: function(id, date){
            dom.byId(id).innerHTML = dateFormatter.format(date);
        }
    });
});

```
### CommonJS
The CommonJS module proposal specifies a simple API for declaring modules server-side and unlike AMD attempts to cover a broader set of concerns such as io, file-system, promises and more.

The format was proposed by [CommonJS](http://www.commonjs.org/) - a volunteer working group which aim to design, prototype and standardize JavaScript APIs. To date they've attempted to ratify standards for both modules and packages.


```js
var lib = require( "package/lib" );

// behaviour for our module
function foo(){
    lib.log( "hello world!" );
}

// export (expose) foo to other modules
exports.foo = foo;
```
- Implemented by node
- Used for the server side when you have modules installed
- No runtime/async module loading
- import via “require”
- export via “module.exports”
- When you import you get back an object
- No tree shaking, because when you import you get an object
- No static analyzing, as you get an object, so property lookup is at runtime
- You always get a copy of an object, so no live changes in the module itself
- Poor cyclic dependency management
- Simple Syntax

### ES Harmony

ES changing periodicly and ES6 works to incorporate many of the ambitious ideas in ES4 and will essentially merge the split between 3.1 and 4. For this reason it was named **"Harmony".**

- Used for both server/client side
- Runtime/static loading of modules supported
- When you import, you get back bindings value (actual value)
- Import via “import” and export via “export”
- Static analyzing — You can determine imports and exports at compile time (statically) — you only have - to look at the source code, you don’t have to execute it
- Tree shakeable, because of static analyzing supported by ES6
- Always get an actual value so live changes in the module itself
- Better cyclic dependency management than CommonJS

-----
