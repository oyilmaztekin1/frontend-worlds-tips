---
title: Contunius Integration
created: '2020-02-03T17:51:47.304Z'
modified: '2020-02-03T17:57:25.997Z'
---

# Contunius Integration

#### resources
- [nodejs react with jenkins ](https://jenkins.io/doc/tutorials/build-a-node-js-and-react-app-with-npm/)
- [jenkins handbook](https://jenkins.io/doc/book/installing/)
- [Docker get started](https://docs.docker.com/get-started/)

### Docker Concepts

#### containers and virtual machines
A container runs natively on Linux and shares the kernel of the host machine with other containers. It runs a discrete process, taking no more memory than any other executable, making it lightweight.


