---
deleted: true
title: Ad Platform Vue fikirleri
created: '2019-12-10T16:53:11.426Z'
modified: '2020-03-03T09:10:29.554Z'
---

# Ad Platform `Vue` fikirleri

- change metodu barındıran bileşenler `inputElementOnchange` gibi bir mixin objesiyle extend edilebilir.
- js component içerisinde [`render`](https://vuejs.org/v2/guide/render-function.html) metodunu kullan.
- slot scope & slot scope with render function...

