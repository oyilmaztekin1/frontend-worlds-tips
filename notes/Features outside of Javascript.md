---
title: Features outside of Javascript
created: '2019-11-15T09:03:43.902Z'
modified: '2019-11-15T09:04:30.735Z'
---

# Features outside of Javascript
- localstorage
- timer
- console
- sessionStorage
- ability to communicate the internet
- XHR requests...
