---
title: Tools
created: '2019-10-22T18:04:51.481Z'
modified: '2019-12-13T05:29:41.445Z'
---

# Tools
- hyperscript
- esm - https://github.com/standard-things/esm The brilliantly simple, babel-less, bundle-less ECMAScript module loader.
- jsdom https://github.com/jsdom/jsdom 
- parcel bundler - zero configuration module bundler https://github.com/parcel-bundler/parcel
- rollup module bundler - https://github.com/rollup/rollup
- path to regexp: https://github.com/pillarjs/path-to-regexp
- monorepo yönetimi: https://nx.dev/
