---
title: Performance
created: '2019-10-31T13:49:23.253Z'
modified: '2019-12-08T09:41:01.133Z'
---

# Performance

### 01 - Web Performance Resources
- preload, prefetch etc...
- [Javascript Memory Management Masterclass](https://speakerdeck.com/addyosmani/javascript-memory-management-masterclass?slide=5)
- [How to write fast, Memory-Efficient JavaScript by Addy Osmani](https://www.smashingmagazine.com/2012/11/writing-fast-memory-efficient-javascript/)
- [All Addy Osmani's notes](https://speakerdeck.com/addyosmani)
- [JavaScript Memory Management](https://blog.sessionstack.com/how-javascript-works-memory-management-how-to-handle-4-common-memory-leaks-3f28b94cfbec)
- [Bellek sızıntısı türkçe c++](http://ibrahimdelibasoglu.blogspot.com/2018/11/c-memory-leak-bellek-sznts-tespiti.html)

### Web Performance Goals
<div style="padding:40px 0;background-color:#283137;text-align:center;color:#efefef;font-weight:bold;">
<p><span style="color:#EC3323;font-weight:bold;"><=200kb</span> (UNCOMPRESSED) INITIAL JAVASCRIPT [TOTAL] </p>
<p> <span style="color:#EC3323;font-weight:bold;"><=100kb</span> (UNCOMPRESSED) INITIAL CSS [TOTAL] </p>
<p>HTTP 1: <span style="color:#EC3323;font-weight:bold;"><= 6 initial network call</span></p>
<p>HTTP 2: <span style="color:#EC3323;font-weight:bold;"><= 20 initial network call</span></p>
</div>

<br/><br/>
> **Split instead of caching...** optimize your code instead of caching..

<br/>

### Script Loaders
The most generic of script loaders (of which there are many) simply create a script tag element and inject it into your page. One of the benefits of loading scripts asynchronously, is that you can load multiple scripts at the same time.

```js
// Very simple asynchronous script-loader

// Create a new script element
var script      = document.createElement('script');

// Find an existing script element on the page (usually the one this code is in)
var firstScript = document.getElementsByTagName('script')[0];

// Set the location of the script
script.src      = "http://example.com/myscript.min.js";

// Inject with insertBefore to avoid appendChild errors
firstScript.parentNode.insertBefore( script, firstScript );

// That's it!
```

> In most browsers, just including a script tag will cause the rest of the page to stop rendering, will wait for the script to load, execute, and then continue on with what it's doing. This is generally bad **(and why you should always include scripts at the bottom, whenever possible)** 

 Asynchronous scripts don't block so you can load more than one at a time. **But:**

 #### Max Number of default simultaneous persistent connections per server/proxy:
 |Browser | limitation per-server/proxy |
 | ------- |---|
 |Firefox 2:  |2|
|Firefox 3+:| 6|
|Opera 9.26: | 4|
|Opera 12:|   6|
|Safari 3:|   4|
|Safari 5:|   6|
|IE 7:|       2|
|IE 8:|       6|
|IE 10:|      8|
|Chrome:|     6|
FYI: this is specifically related to HTTP 1.1; other protocols have separate concerns and limitations (i.e., SPDY, TLS, HTTP 2).

#### How to load your scripts 
Concatenating your scripts and putting them at the bottom of the page is going to give you the biggest jump in performance. But if we load a script asynchronously, we may gain a small advantage by not blocking other things on the page. So some would argue that **asynchronously loading a single concatenated script into your page would be the best performance case**.

I see many people overlook though, is that a script loader is not a replacement for concatenating (and/or minifying) your JavaScript. I see too many sites that have 10 asynchronous parallel requests. This will usually be significantly slower than even just including 10 script tags, I couldn't recommend against it more. **In the real world, 3 scripts loading in parallel seems to be the maximum that anyone has found benefit for.**
