---
attachments: [dynamic-import-splitting (2).png, dynamic-import-splitting (3).png]
title: Webpack
created: '2019-10-24T06:00:14.448Z'
modified: '2019-11-07T08:50:29.929Z'
---

# Webpack
### ???
- [ ] https://webpack.js.org/api/module-methods/#magic-comments
- webpack `preload` `prefetch`
  - [ ] https://twitter.com/mikegreiling/status/993895833933316097
  - [ ] https://webpack.js.org/guides/code-splitting/#prefetchingpreloading-modules
  - [ ] https://medium.com/webpack/link-rel-prefetch-preload-in-webpack-51a52358f84c
-  [ ] `devtool: eval` or `devtool:eval-source-map` makes initial build faster

### !!!
- HMR question by Dan Abramov https://stackoverflow.com/questions/24581873/what-exactly-is-hot-module-replacement-in-webpack
- really useful examples https://github.com/TheLarkInn/webpack-workshop-2018
- https://medium.com/webpack
- https://github.com/TheLarkInn/webpack-developer-kit 
- https://github.com/lawwantsin/webpack-course
- https://github.com/TheLarkInn/webpack-workshop-2018


- Using `UglifyjsWebpackPlugin` makes really big difference.

> !!! Babel kodu commonjs olarak transpile ettiğinde webpack in birçok optimizasyonu geçerli olmuyor. Like tree shaking, scope hoisting etc... Webpack can easly traverse es6 syntax but not compiled and downgraded to es5-3 commonjs. !!! use `module:false` inside of the babel option... 

Acorn doesn't implement proposals under the stage 4... **That is why we need babel to transpile proposals...**

### Core
Tapable is the backbone of the entire architecture. Rest of the whole application is a consisting of bunch of **plugins.** Everything is an OOP in webpack because author of the webpack is a C# developer.
Also using **acorn** as a parser

### how to debug
```json
{
    "debug": "node --inspect --inspect-brk ./node_modules/webpack/bin/webpack.js",
    "prod": "npm run webpack -- --mode production",
    "dev": "npm run webpack -- --mode development",
    "prod:debug": "npm run debug -- --mode production",
    "dev:debug": "npm run debug -- --mode development"
}
```

[dash dash --](https://stackoverflow.com/questions/43046885/what-does-do-when-running-an-npm-command) ne ayak?

### How loaders work?

```js
rules:[
  test: \/.less$/,
  use:["style", "css", "less"]
]

// means left to right invocation and consider that!!!
["style", "css", "less"] ---> style( css( less() ) )
// importing less (less loader) ---> converts that to the css (css-loader) --> converts to the javascript module (style-loader) 

// bad for critical styling or SSR
```

### Code Splitting
first used by [GWT ( google web toolkit)](http://www.gwtproject.org/doc/latest/DevGuideCodeSplitting.html) invented this idea.

According the webpack - Creating seperate chunks of javascript file **at compile time** that will only be loaded asynchronously or parallel

**Two types of code splitting**: static and dynamic. Webpack wont support dynamic spliting... Because it shluld work on runtime but webpack only covers at build time. Dynamic splittning depends on runtime conditions...

If its not initial split it...
<img src="https://gitlab.com/oyilmaztekin1/frontend-worlds-tips/raw/master/attachments/dynamic-import-splitting.png ">
<img src="https://gitlab.com/oyilmaztekin1/frontend-worlds-tips/raw/master/attachments/dynamic-import.png ">


### Webpack JSONP
Webpack loads all chunks and their reference into webpackJsonp object which located under the `window` global object. This object holds js functions and their reference js files and its `push` method. By this `push` method it creates new chunks... 

```js
window.webpackJsonp
```

### Lazy Loading

Lazy loading in webpack can be handled by the `code splitting` and `dynamic import` itself. Split your code and load it when you need it

```javascript
const loadFooter = () => import("./footer); // dynamic import code


btn.addeventListener("click", e => {
  loadFooter().then(m => {
    document.body.appendChild(m.footer);
  })
})

```

Vue has a natural first-class code-splitting... Great implementation...
React has `react-loadable` but its not first-class...

### How HMR and Update Chunks works
<img src="https://webpack.github.io/assets/HMR.svg">
