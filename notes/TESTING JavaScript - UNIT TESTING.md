---
title: TESTING JavaScript - UNIT TESTING
created: '2020-02-02T17:13:18.791Z'
modified: '2020-02-03T11:42:58.838Z'
---

# TESTING JavaScript - UNIT TESTING

### tutorials
- https://coursehunter.net/course/principy-i-praktika-testirovaniya
- https://coursehunter.net/course/testirovanie-react-komponentov-s-enzyme-i-jest

### JSDOM 
JSDOM - https://www.youtube.com/watch?v=T5uhjR6rZMU
https://airbnb.io/enzyme/docs/guides/jsdom.html
https://github.com/jsdom/jsdom


console.log için `wrapper.debug()`

#### first configure the adapter
```js
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
```


### `shallow` Rendering
is for testing units not for rendering full-dom. It shows jsx output of the component. This means components aren't mounted yet. note that: **ensure your tests aren't indirectly asserting on behavior of child component.** Shallow automaticly calls lifecycle methods such as `componentDidMount` and `componentDidUpdate` If you want to prevent add `disableLifeCycleMethods:true` parameter. 

```js
it('renders three <Foo /> components', () => {
  const wrapper = shallow(<MyComponent />,  { context: {}, disableLifeCycleMethods: true } );    
});
```

### Full-Dom Rendering with `mount()`
unlike shallow or static rendering, full rendering **actually mounts the component in the DOM**, which means tests can affect each other if they are using or depending on the same DOM. 

Full DOM rendering requires Full-DOM API. this means that it must be run on the browser or browser-like environment. If you don't want to run your test in browser env use `jsdom` headless browser that provides DOM API's. 

note that: if you want to test then want to remove it after use `unmount()` for cleaning the DOM.
 

### testing props

```js
const wrapper = shallow(<Link adress="www.google.com" />)
expect(wrapper.instance().props.adress).toBe("www.google.com);
```

### testing first item
```js
const wrapper = shallow(<Link adress="www.google.com" />)
expect(wrapper.find('a').length).toBe(1);
wrapper.setProps({hide:true})
expect(wrapper.find('a').length).toBe(1); ❌
expect(wrapper.get(0)).toBeNull(); ✅ 
```

### Utilities
```js
/**
 * Factory function to create a ShallowWrapper for the App component.
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @param {object} state - Initial state for setup.
 * @returns {ShallowWrapper}
*/
const setup = (component, props, state) => {
  const wrapper = shallow(<component {...props} />)
  if (state) wrapper.setState(state);
  return wrapper;
}

const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
}


```

### testing state changes and DOM
##### Click event
```js
render(){
return (
  <>
    <div class="button-state"> 
      { this.state.on ? "Yes!" : "No!" }
    </div>
    <button onClick={() => this.setState({ on:true })}>
  </>
  )
}

// test
it('asdasd', () => {
  const wrapper = shallow(<App />);
  const button = wrapper.find('button');
  expect(wrapper.find('.button-state').text()).toBe('No!');
  button.simulate('click);
  expect(wrapper.find('.button-state').text()).toBe('Yes!');
})
```

##### Change event
```js
render(){
return (
  <>
    <h2> {this.state.input} </h2>
    <input onChange={(e) => this.setState({ input:e.currentTarget.value })}>
  </>
  )
}

// test
it('asdasd', () => {
  const wrapper = shallow(<App />);
  const input = wrapper.find('input');
  expect(wrapper.find('h2').text()).toBe('');
  button.simulate('change', {currentTarget: {value: "Ozer" }});
  expect(wrapper.find('h2').text()).toBe('Ozer');
})
```

### testing lifecycle methods

```js
// ComponentDidMount
it('calls componentDidMount', () => {
  jest.spyOn(App.prototype, "componentDidMount");
  const wrapper = shallow(<App />);
  expect(App.prototype.componentDidMount.mock.calls.length).toBe(1);
})

// ComponentWillReceieveProps
it('calls componentWillRecieveProps', () => {
  jest.spyOn(App.prototype, "componentWillRecieveProps");
  const wrapper = shallow(<App />);
  wrapper.setProps({hide:true});
  expect(App.prototype.componentWillRecieveProps.mock.calls.length).toBe(1);
})
```

### testing instance methods

```js

class App extends Component{
  handleStrings(str){
    return !!str;
  }
}

it('handleStrings functions returns correctly', () => {
  const wrapper = shallow(<App />);
  expect(wrapper.instance().handleStrings("hello world")).toBe(true);
  expect(wrapper.instance().handleStrings("")).toBe(false);
})
```

### testing input

```js
const updateInput = (wrapper, selector, newValue) => {
  const input = wrapper.find(instance);
  input.simulate('change', {
    currentTarget: {
      value: newValue
    }
  })
  return wrapper.find(instance);
}

test("allows user to fill out form", () => {
  const wrapper = shallow(<Form/>)
  const nameInput = updateInput(wrapper, "#nameInput", "Ozer");
  const emailInput = updateInput(wrapper, "#emailInput", "test@test.com");

  expect(nameInput.props().value).toBe("Ozer");
  expect(emailInput.props().value).toBe("test@test.com);
})

```

### testing formSubmit

```js
test("allows user to fill out form", () => {
  jest.spyOn(api, "addUser").mockImplementation(() => {
    Promise.resolve({
      data: "New User Added"
    });
  })
  const wrapper = shallow(<Form/>)
  updateInput(wrapper, "#nameInput", "Ozer");
  updateInput(wrapper, "#emailInput", "test@test.com");
  wrapper.find("#addUserButton").simulate("submit", {preventDfault: () => {}})

  expect(api.addUser).toHaveBeenCalledWith("Ozer","test@test.com");
})
```

### __mock__ manuel
sometimes you need to mock post request with response. In these cases manuel __mock__ can be used.  

