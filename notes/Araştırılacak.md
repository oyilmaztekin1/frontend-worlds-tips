---
title: Araştırılacak
created: '2019-11-14T06:20:39.981Z'
modified: '2019-12-13T11:53:16.811Z'
---

# Araştırılacak

- [ ] schema-driven design system or development
- [ ] javascript first edition spec - https://www.ecma-international.org/publications/files/ECMA-ST-ARCH/ECMA-262,%201st%20edition,%20June%201997.pdf
- [ ] dangling pointers
- [ ] buffer overruns
- [ ] raw pointers (rust)
- [ ] a C foreign function interface (rust)
- [ ] inline assembly (rust)
- [ ] CSS Unit Testing - https://github.com/jamesshore/quixote
- [ ] `bash install.sh` vs `npm install` and `npm config set ignore-scripts true`
- [ ] problem about null class : https://github.com/tc39/ecma262/pull/1321
- [ ] [Webpack Closure Compiler](https://www.npmjs.com/package/webpack-closure-compiler)
- [x] Error handling - try, catch 

**react leaks** (vs svelte)
- [ ] `shouldComponentUpdate`
- [ ] `React.PureComponent`
- [ ] `useMemo`
- [ ] `useCallback`


## tools used in other
- findbugs, pmd etc... static code analyzer. how are these tools finding bugs??? can be implemented with js? OR **SonarQube** supports `ts` and `js` how to improve eslint approaching...
