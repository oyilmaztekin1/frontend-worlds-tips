---
title: Chrome Dev Tools
created: '2020-02-02T17:00:13.614Z'
modified: '2020-02-02T17:00:33.530Z'
---

# Chrome Dev Tools
Herhangi bir event işleminde durdurmak için: 
`Sources -> Event Listener Breakpoint || XHR/fetch breakpoints `

DOM değişikliklerini gözlemlemek için
`Elements -> sağ click -> break on -> …any`

