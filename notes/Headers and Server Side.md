---
title: Headers and Server Side
created: '2019-12-08T09:36:21.650Z'
modified: '2019-12-08T09:36:23.471Z'
---

# Headers and Server Side
- https://schepp.github.io/HTTP-headers
### Compressions
`.gz` stans for `gzip` `.br` stands for `brotli`. MOst websites uses gzip instead of brotli because brotli is so slow but **brotli makes files smaller**. Server respond fast with Gzip algorithm. https://blogs.akamai.com/2016/02/understanding-brotlis-potential.html 

### Image Optimization
`Accept-CH` is very powerful but works only with **HTTPS**. Most of the browser and IE doesn't support that. You can get size of the device then serve the image based on that size data. https://www.youtube.com/watch?v=md7Ua82fPe4&t=2s 

`Save-Data: on` or `Vary: Save-Data` indicates the client's preference for reduced data usage. This could be for reasons such as high transfer costs, slow connection speeds, etc. HTML, CSS and JS get minified with this flag.  `Save-Data` mode can be detected via `JavaScript`, either

```js
if (
  navigator.connection && 
  navigator.connection.saveData === true
) {
  // Implement data saving operations here.
}
```

### use `Cache-Control` instead of `Expires`
`Cache-Control` was introduced in HTTP/1.1 and offers more options than `Expires`.  They can be used to accomplish the same thing... `Expires` is recommended for static resources like **images** and `Cache-Control` when you need **more control over how caching is done**

Although the `Expires` header is useful, it has some limitations. First, because there’s a date involved...

Another problem with `Expires` is that it’s easy to forget that you’ve set some content to expire at a particular time. If you don’t update an `Expires` time before it passes, each and every request will go back to your Web server.

```
Cache-Control: max-age=31536000, public, immutable
```

**`max-age=31536000`**: Resource expires one year from now

**`public`**: Proxies are allowed to cache the resource

**`immutable`**: Resource shall never be checked agai

### `Clear-Site-Data`
If a user signs out of your website or service, you might want to remove locally stored data. You can achieve that by adding the Clear-Site-Data header when sending the page confirming that logging out from the site has been accomplished successfully (https://example.com/logout, for example):

```
Clear-Site-Data: "cache", "cookies", "storage", "executionContexts"
```

### Client Hints (performance)
Client Hints use **must be activated/opted in by the server**:

```py
Accept-CH: DPR, Width, Viewport-Width, Device-Memory
Accept-CH-Lifetime: 86400  #for the next 24 hours
```

Which means that the first call to a server will always be w/o Client Hints

Client Hints allow for proactive content negotiation. They can tell the server about a device's...

- **`DPR`**: 1.5: Device pixel ratio (Screen pixels per CSS pixel)
- **`Viewport-Width`**: 1024: Width of the viewport in pixels
- **`Width: 400`**: Actual rendered width of an image in CSS pixels
- **`Device-Memory`**: 0.5: The devices's main memory in GiB
- **`autoplay`**: Allow or disallow autoplaying video
- **`sync-script`**: Allow or disallow synchronous script tags
- **`document-write`**: Allow or disallow document.write
- **`lazyload`**: Force all images and iframe to load lazily (as if all had `lazyload="on"` attribute set)
- **`image-compression`**: restrict images to have a byte size no more than 10x bigger than their pixel count
- **`maximum-downscaling-image`**: restrict images to be downscaled by not more than 2x
- **`unsized-media`**: requires images to have a width & height specified, otherwise defaults to 300 x 150
- **`layout-animations`**: turns off CSS animation for any property that triggers a re-layout (e.g. top, width, max-height)

### Resource Hints
Resource Hints help the browser with discovering render critical resources, like `CSS`, `fonts` and maybe also `JavaScript`.

```html
<!--Most of the time you see them in the markup, like so: -->
<link href="/styles.css" rel="preload" as="style">

<!-- But you can also send that info via HTTP header: -->
Link: <https://domain.com/styles.css>; rel=preload; as=style
```


### Table
##### prefetch, preload everything can be set by backend inside of the header includind critical resources. geolocation API can be blocked with `Feature-Policy`...

| flag | desc |
| --- | ----- |
| `Strict-Transport-Security`| only access with `HTTPS`|
| `Content Security Policy (CSP)` | represents the resources the user agent is allowed, `<iframe>` `<links>` `XMLHttpRequest` etc... |
| `cache-control`| specify directives for caching mechanisms in both requests and responses. browser also revalidates if the resources changed. [view more](https://csswizardry.com/2019/03/cache-control-for-civilians/) | 
| `Save-Data: on` or `Vary: Save-Data` | indicates the client's preference for reduced data usage. This could be for reasons such as high transfer costs, slow connection speeds, etc. HTML, CSS and JS get minified with this flag |
| `Cache-Control: no-transform` | protect certain assets from being transformed / optimized |
| `Clear-Site-Data: <token>` | header clears browsing data |
