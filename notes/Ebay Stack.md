---
title: Ebay Stack
created: '2020-02-25T08:18:00.656Z'
modified: '2020-02-27T07:26:59.345Z'
---

# Ebay Stack 

[16:08, 26.02.2020] Oğuz Kılıç: react
next.js
graphql
apollo client & server & gateway
styled-components
react context & hooks
jest & enzyme
webpack

tadında bi stack var.
[16:08, 26.02.2020] Oğuz Kılıç: daha önce bahsetmiştim zaten
[16:08, 26.02.2020] Oğuz Kılıç: design system, bff (graphql), core kısımlarından olusan bi yapı kurduk.
[16:09, 26.02.2020] Oğuz Kılıç: bff'lerde %100 test coverage var unit & integration testler yazıyoruz. design system'de sadece unit var %90 civarında.
[16:09, 26.02.2020] Oğuz Kılıç: app tarafında deadline'dan dolayı test yazmadık ama böyle 2-3 günde tamamlancak bi kod yapısı var.
[16:10, 26.02.2020] Oğuz Kılıç: instana, jenkins CI / CD, prometheus & grafana, splunk, sentry gibi uçtan uca her yere entegre çalışıyoruz.
[16:11, 26.02.2020] Oğuz Kılıç: tüm uygulama dockerize ve kubernetes üzerinde koşuyor.
[16:12, 26.02.2020] Oğuz Kılıç: benim backlog'umda circuit breaker, type-safety gibi konular var. muhtemelen önce ordan gireriz.
