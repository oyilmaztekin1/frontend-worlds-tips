---
title: Interview words and concepts
created: '2019-04-25T21:59:13.131Z'
modified: '2019-12-08T10:04:52.717Z'
---

# Interview words and concepts

- **`FIFO`** : for stack: first in first out
- **`thread of execution`**
- **`frame`**: function or stack frames are arranged in stack. recencly called **always on the top**
- **`call stack`**: when a function is invoked also new frame pushed onto( top of the) stack. ( new frame creation). Whwn a function finishes its work, its frame **popped** of the stack. 
- **`const`**: piece of data we are not allowed to change its adress or position inside of the memory  

## What is difference between concurrency and parallelism
**Concurrency** is when two or more tasks can start, run, and complete in overlapping time periods. It doesn't necessarily mean they'll ever both be running at the same instant. For example, multitasking on a single-core machine.

> Dealing lot of things at once...

```sh
# successively and step by step
------              ----         ____
    -----       ----    ----->  |Core|   
          -----                 |____|
```

**Parallelism** is when tasks literally run at the same time, e.g., on a multicore processor.

> Doign lot of things at once...

```sh
# simultaneously
------        ----------  ----------> thread
-------    ----------  -------------> thread
------------- ----------   ---------> thread
```

#### resource
- https://learning.oreilly.com/videos/concurrent-and-parallel/9781771375313
