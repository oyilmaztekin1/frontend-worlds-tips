---
title: Dönüşüm meseleleri (SSR etc..)
created: '2019-11-16T09:10:19.282Z'
modified: '2019-12-08T09:37:08.292Z'
---

# Dönüşüm meseleleri (SSR etc..)

## Nodejs Ve SSR Problemleri
- prettier, eslint, babel, webpack, typescript, jest tamamı farklı organizasyonlar tarafından yazılıyor. Uyum problemleri çözülmeye çalışsa da yeni bir feature, diğer paketi bloklayabiliyor. 
- test farklılıkları... Typescript, react, vue es5, es6 hepsi farklı test ediliyor.
- Hello world api si **19MB** ile çalışır. Aynı api rust da **1MB**

- .net Core [Reactjs.net](https://reactjs.net/) kullanıyor. kullandığı [v8 engine](https://github.com/Taritsyn/JavaScriptEngineSwitcher)
- **server-side fetch** ( for SEO initial data ve initial props )
- server tarafında oluşan dataların **JS** tarafına yansıtılması ( initial data and props )
- iletişim için isomorphic (client and backend aynı dil)
- Client side library lifecyles
- performans template engine yorumlar gibi v8 **JS** yorumluyor  
- CPU ve memory tüketimi (nodejs vs others...)
- v8 heap ownership [threading example](https://dflemstr.github.io/v8-rs/v8/index.html)
- v8 releases (proposals ESNext)...
- v8 kodun sunucuda derlenme maliyeti 
- **binary** koşmak (wasm)
- derlenme yükü arttıkça prod süreçlerinin kısalması
- harici js
- dahili kodu js e çevirme

## Sorular
- [ ] Error handling and logging client side 

