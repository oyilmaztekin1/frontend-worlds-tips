---
deleted: true
title: draft
created: '2019-11-05T13:02:20.162Z'
modified: '2019-11-05T13:16:51.571Z'
---

# draft 

### webpack.prod.conf.js
```js
'use strict'
const path = require('path')
const utils = require('./utils')
const webpack = require('webpack')
const config = require('../config')
const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.base.conf')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const conf = config();
const env = require('../config/prod.env')

const webpackConfig = merge(baseWebpackConfig, {
  module: {
    rules: utils.styleLoaders({
      sourceMap: conf.build.productionSourceMap,
      extract: true,
      usePostCSS: true
    })
  },
  devtool: conf.build.productionSourceMap ? conf.build.devtool : false,
  output: {
    path: conf.build.assetsRoot,
    filename: utils.assetsPath('js/[name].[chunkhash].js'),
    chunkFilename: utils.assetsPath('js/[id].[chunkhash].js')
  },
  optimization: {
    splitChunks: {
      chunks: 'async',
      minSize: 30000,
      maxSize: 0,
      minChunks: 3,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: '~',
      automaticNameMaxLength: 30,
      name: true,
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        }
      }
    }
  },
  plugins: [
    // http://vuejs.github.io/vue-loader/en/workflow/production.html
    new webpack.DefinePlugin({
      'process.env': env
    }),
    new UglifyJsPlugin({
      uglifyOptions: {
        compress: {
          warnings: false
        }
      },
      sourceMap: conf.build.productionSourceMap,
      parallel: true
    }),
    // extract css into its own file
    new ExtractTextPlugin({
      filename: utils.assetsPath('css/[name].[hash].css'),
      // Setting the following option to `false` will not extract CSS from codesplit chunks.
      // Their CSS will instead be inserted dynamically with style-loader when the codesplit chunk has been loaded by webpack.
      // It's currently set to `true` because we are seeing that sourcemaps are included in the codesplit bundle as well when it's `false`,
      // increasing file size: https://github.com/vuejs-templates/webpack/issues/1110
      allChunks: true,
    }),
    // Compress extracted CSS. We are using this plugin so that possible
    // duplicated CSS from different components can be deduped.
    new OptimizeCSSPlugin({
      cssProcessorOptions: conf.build.productionSourceMap
        ? { safe: true, map: { inline: false } }
        : { safe: true }
    }),
    // generate dist index.html with correct asset hash for caching.
    // you can customize output by editing /index.html
    // see https://github.com/ampedandwired/html-webpack-plugin
    new HtmlWebpackPlugin({
      filename: conf.build.index,
      template: 'index.html',
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
        // more options:
        // https://github.com/kangax/html-minifier#options-quick-reference
      },
      // necessary to consistently work with multiple chunks via CommonsChunkPlugin
      chunksSortMode: 'dependency'
    }),
    // keep module.id stable when vendor modules does not change
    // new webpack.HashedModuleIdsPlugin(),
    // enable scope hoisting
    // new webpack.optimize.ModuleConcatenationPlugin(),

    // see: https://webpack.js.org/plugins/commons-chunk-plugin/#extra-async-commons-chunk
    // copy custom static assets
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, '../static'),
        to: conf.build.assetsSubDirectory,
        ignore: ['.*']
      }
    ])
  ]
})

if (conf.build.productionGzip) {
  const CompressionWebpackPlugin = require('compression-webpack-plugin')

  webpackConfig.plugins.push(
    new CompressionWebpackPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: new RegExp(
        '\\.(' +
        conf.build.productionGzipExtensions.join('|') +
        ')$'
      ),
      threshold: 10240,
      minRatio: 0.8
    })
  )
}

if (conf.build.bundleAnalyzerReport) {
  const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
  webpackConfig.plugins.push(new BundleAnalyzerPlugin())
}

module.exports = webpackConfig
```

### index.js

```js
"use strict";
// Template version: 1.3.1
// see http://vuejs-templates.github.io/webpack for documentation.
const { sellerOfficeEndPoints, apiTargetsEndPoints, createProxyTable } = require("./endPoints");
const path = require("path");

module.exports = function() {
  const enviroment = process.env.NODE_ENV;
  const apiTarget = apiTargetsEndPoints[enviroment];
  const mallfrontTarget = sellerOfficeEndPoints[enviroment];
  return {
    dev: {
      // Paths
      assetsSubDirectory: "static",
      assetsPublicPath: "/",
      // creates proxy enpoints based on the environment parameter
      proxyTable: createProxyTable(apiTarget, mallfrontTarget),

      // Various Dev Server settings
      host: "localhost", // can be overwritten by process.env.HOST
      port: 8088, // can be overwritten by process.env.PORT, if port is in use, a free one will be determined
      autoOpenBrowser: false,
      errorOverlay: true,
      notifyOnErrors: true,
      poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-

      /**
       * Source Maps
       */

      // https://webpack.js.org/configuration/devtool/#development
      devtool: "cheap-module-eval-source-map",

      // If you have problems debugging vue-files in devtools,
      // set this to false - it *may* help
      // https://vue-loader.vuejs.org/en/options.html#cachebusting
      cacheBusting: true,

      cssSourceMap: true
    },

    build: {
      // Template for index.html
      index: path.resolve(__dirname, "../dist/index.html"),

      // Paths
      assetsRoot: path.resolve(__dirname, "../dist"),
      assetsSubDirectory: "static",
      assetsPublicPath: "/",

      /**
       * Source Maps
       */

      productionSourceMap: true,
      // https://webpack.js.org/configuration/devtool/#production
      devtool: "#source-map",

      // Gzip off by default as many popular static hosts such as
      // Surge or Netlify already gzip all static assets for you.
      // Before setting to `true`, make sure to:
      // npm install --save-dev compression-webpack-plugin
      productionGzip: true,
      productionGzipExtensions: ["js", "css"],

      // creates proxy enpoints based on the environment parameter
      proxyTable: createProxyTable(apiTarget, mallfrontTarget),

      // Run the build command with an extra argument to
      // View the bundle analyzer report after build finishes:
      // `npm run build --report`
      // Set to `true` or `false` to always turn it on or off
      bundleAnalyzerReport: process.env.npm_config_report
    }
  };
};

```
