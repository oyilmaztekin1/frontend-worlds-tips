---
title: Vue Advanced Tips
created: '2019-12-12T06:36:28.310Z'
modified: '2019-12-12T11:43:08.623Z'
---

# Vue Advanced Tips
- props vs $attrs
- in attachments folder `vue-desiging-advanced-components.txt` (designing advanced components)
- https://github.com/chrisvfritz/vue-enterprise-boilerplate

use functional (render) component to solve root problem in small component

### `watch` tips 
function name can be used inside instead of writing function

```js
 methods: {
    toggleSelectProduct(){
      // do something...
    },
  },
  watch: {
    propName: "toggleSelectProduct",
    immediate: false // if true, will be immediately invoked after created
  }
```

### dynamic listener
```html
<!-- in parent -->

<BaseInput placeholder="asdasd" @focus="doSomething">

<!-- in baseInput comp -->
<input :value="value" v-on="listeners">
```

```js
computed:{
  listeners(){
    return {
      ...this.$listeners,
      input: event => this.$emit("input", event.target.value)
    }
  }
}
```

#### full dynamic version of listener (transparent wrapper)

```html
<template>
  <label>
    {{ label }}
    <input v-bind="$attrs">
  </label>
</template>
```

```js
{
  inheritAttrs: false
}
```

## Inject/Provide
- attention about reactivity
```js
provide(){
  const newObj = {};
  Object.defineProperty(newObj, 'reactiveDataProperty', {
    enumarable: true,
    get: () => this.reactiveDataProperty
  })
  return { newObj }
  // must return object, use as object or function returns an object
}

//or use reactiveData into Object
data(){
  reactiveData:{
    reactiveProperty: 1
  }
},
provide(){
  return {
    this.reactiveData.reactiveProperty
  }
}

```
