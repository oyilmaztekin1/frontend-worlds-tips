---
title: Markdown Checker
created: '2019-11-11T10:09:48.946Z'
modified: '2019-11-11T15:40:08.549Z'
---

# Markdown Checker
- **(Markdown Specs)** https://github.github.com/gfm/#full-reference-link (markdown expression types)
- **(example markdown)** https://astexplorer.net/#/gist/51df6d1a522d5a08c3017d92549345f9/ac9ddaca577f5115e7e220c74acbe786ab3a4a11 
- **(search in remark)** https://github.com/remarkjs/remark/search?l=JSON&p=2&q=linkReference

- tableCell includes elements that we need

all children types are `Array`
- if table has more than one table
  - create table object
  - iterate table
  - send each table to `detectHeader` function
  - check type of node is `tableRow` if `false` return && `tableRow` has children if `false` return
    - iterate `tableRow.children` 
    - check type of node is `tableCell` if `false` return && `tableCell` has children if `false` return
      - iterate `tableCell.children``
      - if html or text `getHTMLorTextValue()`
      - if `tableCell.children` has children 
        - iterate `tableCell.children.children`
        - if html or text `getHTMLorTextValue()`
        - if type `linkReference`
          - if `referenceType` === `full` && `findDefinitionAndUse(node)`
          - else `getHTMLorTextValue()`


### `getHTMLorTextValue(node)`
- if type `html` or `text`
  - return value
- else
  - return

### `findDefinitionAndUse(node)`
- go find that link definition inside of the definition hash with `node.identifier`
- `node.children` && iterate `node.children`
  - `getHTMLorTextValue(node)`



## Doc

### linkReference
- **`full`** also known as `definition`
  - `[regexp-legacy]: https://github.com/tc39/proposal-regexp-legacy-features `
- **`shortcut`**
  - `[shortcut](http://github.com)`
- **`collapsed`**
  - `[foo]: /url "title"` 



### link definition represantation
```js
// [regexp-legacy]: https://github.com/tc39/proposal-regexp-legacy-features 
{
  "type": "linkReference",
  "identifier": "regexp-legacy",
  "label": "regexp-legacy",
  "referenceType": "full",
  "children": [
    {
      "type": "text",
      "value": "Legacy RegExp features in JavaScript",
      "position": {
        "start": {
          "line": 20,
          "column": 4,
          "offset": 1353
        },
        "end": {
          "line": 20,
          "column": 40,
          "offset": 1389
        },
        "indent": []
      }
    }
  ],
  "position": {
    "start": {
      "line": 20,
      "column": 3,
      "offset": 1352
    },
    "end": {
      "line": 20,
      "column": 56,
      "offset": 1405
    },
    "indent": []
  }
}
```

