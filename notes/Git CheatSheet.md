---
title: Git CheatSheet
created: '2019-10-23T16:41:33.053Z'
modified: '2019-11-05T06:37:06.247Z'
---

# Git CheatSheet

```sh
# sets user name and email globally
git config --global user.name [name]
git config --global user.email [email address]

# sets user name and email locally
git config --local user.name [name]
git config --local user.email [email address]
```

### Logging
| command | ? | 
|-------|----------|
| `git reflog`| you'll see everything you done|
| `git cat-file -p commithashcode`| |
| `git log --oneline --decorate `| you'll see log with one line and branch and tag details |
| `git log --graph`| you'll see log with branching graphic |
| `git count-objects -H`| object size and |
| `cat .git/HEAD` | see where is head |
| `git log --grep="first"` | search by commit message |
| `git log --pretty=format:"%h - %an, %ar : %s"`| list with hash, author, author date, subject(message)|
| `git show --pretty="" --name-only hashID` | shows committed files |
e72b36dc2007bf286fd599d321cba1b6c4bd1ceb

#### order by date

```sh
git log --after="2014-7-1  
git log --after=“yesterday"
git log --after="2014-7-1" --before="2014-7-4"
```


### History Manipultation
| command | ? | 
|-------|----------|
| `git reset --hard HEAD@{index}`| moves head to the index point and resets code.. |
| `git reset HEAD~ --soft`| moves head to the last commit also leaves the changes |
| `git revert [commitId]` | reverts changes |
| `git branch -m "yeni-branch-adi"` | changes branch name|
| `git merge --abort` | stop and abort merge |
| `git rebase --abort` | stops rebase |
| `git cherry-pick commithash` | get committed changes inside of a commit |

```sh
git rev-parse HEAD
```
gives SHA1 hash code of HEAD commit


## Oh Shit Cases

### Stashed
```sh
#add changes to the stash
git stash
#add changes to the stash with message
git stash save "Message"
#list stash 
git stash list
#see stashed code 
git stash show -p stash@{0}
#see all stashes 
git stash show -p 
```

### committed to the wrong branch
```sh
# undo the last commit, but leave the changes available
git reset HEAD~ --soft
git stash

# move to the correct branch
git checkout name-of-the-correct-branch
git stash pop
git add . # or add individual files
git commit -m "your message here";
# now your changes are on the correct branch
```
**or just cherry-pick**

### I need to undo my changes to a file!
```sh
# find a hash for a commit before the file was changed
git log
# use the arrow keys to scroll up and down in history
# once you've found your commit, save the hash
git checkout [saved hash] -- path/to/file
# the old version of the file will be in your index
git commit -m "Wow, you don't have to copy-paste to undo"
```

### Fuck this noise, I give up.
For real though, if your branch is sooo borked that you need to reset the state of your repo to be the same as the remote repo in a "git-approved" way, try this, but beware these are destructive and unrecoverable actions!


```sh
# get the lastest state of origin
git fetch origin
git checkout master
git reset --hard origin/master
# delete untracked files and directories
git clean -d --force
# repeat checkout/reset/clean for each borked branch
```

