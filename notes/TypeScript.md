---
title: TypeScript
created: '2020-02-07T15:27:18.295Z'
modified: '2020-02-14T07:25:57.206Z'
---

# TypeScript

### questions 
- [ ] [difference between interface and abstract class](https://www.google.com/search?q=difference+between+abstact+and+interface&oq=difference+between+abstact+and+interface&aqs=chrome..69i57j0l7.13228j0j7&sourceid=chrome&ie=UTF-8)

### repeat it
- typescript code review - https://coursehunter.net/course/sozdanie-obektno-orientirovannogo-koda-typescript

### videos
- [Typescript: The Complete Developer's Guide](https://coursehunter.net/course/typescript-polnoe-rukovodstvo-razrabotchika)
- [Up and Running with TypeScript](https://coursehunter.net/course/pristupaem-k-rabote-s-typescript)
- [Practical Advanced TypeScript](https://coursehunter.net/course/advanced-typescript-praktika)
- [Creating Object-oriented TypeScript Code](https://coursehunter.net/course/sozdanie-obektno-orientirovannogo-koda-typescript)

### [`abstact`](https://www.typescriptlang.org/docs/handbook/classes.html#abstract-classes)
- they are base classes can ne derived by other class 
- cannot be constructed only extends another class
- unlike interface can be contain detail information about its members
- `abstract` expression can be used for determining a member or a class as an **abstract**  
- abstact methods share similiar syntax for defining implementation

```ts
abstract class Department {

    constructor(public name: string) {
    }

    printName(): void {
        console.log("Department name: " + this.name);
    }

    abstract printMeeting(): void; // must be implemented in derived classes
}
```
### interface

Interfaces can be used to enforce consistency across different classes... Defined methods in interface can be used in implemented class. 

> when the objects or classes exibit the same behavior but in a different way (polymorphism)

- Interfaces can be used to create custom data type ( like `type` declaration )
- Classes can implement interfaces which can help to create consistency across an application
- Interfaces are only used during development ( no any impact on bundle size, only compliler reads that rules )
- In addition to `type` you can declare how function should be used in interface.

### difference between type and interface
- type aliases can act sort of like interfaces, however, there are 3 important differences ( union types, declaration merging)
- use whatever suites you and your team, just be consistent
- always use interface for public API's definition when authoring a library or 3rd party ambient type definitions 
- Use interface for creating class reference and abstraction 
- consider using type for your React Component Props and State
- https://stackoverflow.com/questions/37233735/typescript-interfaces-vs-types/52682220#52682220
- https://medium.com/@martin_hotell/interface-vs-type-alias-in-typescript-2-7-2a8f1777af4c

- ❌ with **type**, Decleration merging doesn't work 
- ❌ with **type** union types cannot be implemented on a class


#### ❌ Decleration merging
```ts
// two of them below will be merged
interface Box{
  x: number,
  y: number
}

interface Box{
  scale: number
}

// ❌ doesn't work with type alias
const box: Box = {x :3, y:5, scale:0.1;} 
```

#### ❌ union types (same for extending)

```ts
interface Shape {
  area() : number
}

type Perimeter = {
  perimeter(): number
}

type RectangleShape = (Shape | Perimeter) & Point

// Tslint will underline RectangleShape because; type alias union cannot be used for implements
class Rectangle implements RectangleShape{

}
```

### Public, private, and protected modifiers
#### [resource](https://www.typescriptlang.org/docs/handbook/classes.html#public-private-and-protected-modifiers) 
Members are `public` by default.

`protected` members are can be accessed within derived class but cannot be reached by instance object constructed from derived class

note that: cannot be accessed form outside of the class like `instance.protectedMember` throws an error.

```ts
class Vehicle{
  color: string;
  constructor(color: string){
    this.color = color;

  }
}
const vehicle = new Vehicle("orange");

// alternative usage with public keyword
class Vehicle{
  constructor(public color: string){

  }
}
const vehicle = new Vehicle("orange");


class Car extends Vehicle{
  constructor(public wheels:number, color:string){
    super(color); // for parent class's color properties
  }
}

const car = new Car(4, "red");
```

### `never`
 - if there is a change to return something like throwing error then `void` can be used instead
 - `never` means function never returns anything in any condition
 - `never`: No type is a subtype of or assignable to never (except never itself).
 - `never`: Function returning never must have unreachable end point
 - In contrast between void and never,  the `never` is a type containing no values, which means that a function with this return type can never return normally at all. This means either throwing error and exception or failing to terminate.


#### resources
- https://github.com/typescript-cheatsheets/react-typescript-cheatsheet
- https://www.sohamkamani.com/blog/react/2018-08-22-using-react-with-typescript/#defining-event-handlers-for-elements
