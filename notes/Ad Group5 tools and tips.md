---
deleted: true
title: Ad Group5 tools and tips
created: '2019-10-23T06:14:58.587Z'
modified: '2020-03-03T09:10:27.094Z'
---

# Ad Group5 tools and tips
- **Endpointler**: https://storc.n11.local:8443/console/project/dmall-was-dev/browse/deployments
  - **user**:developer
  - **password**:Qazxsw123
- **Uygulama** : https://storc.n11.local:8443/console/project/dmall-web-dev/overview
- **test**: https://storc.n11.local:8443/console/project/dmall-web-test/overview
- **Jenkins** - http://172.18.184.89:8080/job/WEB/job/Apps/job/Ad%20Platform%20Client/


### Eksikler
- [x] Webpack alias ve görünüm farklılıkları düzenlenmesi lazım. 
- [ ] 404 ve 500 sayfası
- [ ] `DataTable` faruk'un yazdığı table markup ile güncellenecek.
- [ ] **Projede filtrelemeler için form, listelemeler için datatable lazım. Bu ikisi reusable komponent olmalı.**
- [ ] ad-analytics-query sellerID cookie olarak gitmesi lazım `api/ad-analytics-query/{sellerId}`

### Adproducts.vue component

- **`selected`**: component state... **fix**: can be setted as a constant or immutable clone of `selectedProducts`
  - When assinged something on this also it is dispatched to the VUEX state. Change with `selectedProducts`
- **`checkedProducts`** coming from `getters`
- <span style="text-decoration: line-through;">**`selectedProducts`** coming from `VUEX`</span> use getter instead
- **`isAllSelected`** all listed products will be mapped if "tümünü seç" btn checked 

#### DataTables
- [ ] ismini index yerine SponsoredProductAnalyze.vue yap
- [ ] `ListManager` komponentini `DataTable` içerisine aktarıyoruz.
- `store` mantığını sayfa bazlıdan endpoint bazlıya dönüştürüyoruz.
- bundle size büyük. chartjs yerine daha lightweight...

### 7 Kasım
- [x] `webpack.prod.conf.js` dosyası içerisindeki tüm pluginleri tek tek incele, sil... 
- [x] `webpack.prod.conf.js` içinden `GZip` ayarlarını sil

- `updateSingleDraftProduct` update the 
- `getDraftProducts` call the action
- `getActiveDraftProducts` then state from getter...
- `historyCurrentPoint: state => state.sponsoredShop.historyPoint.currentPoint` >! `bid` büyük olamaz
- `bid` >! `maxDailyBugdet`büyük olamaz `getActiveDraft.maxDailyBudget`
 
